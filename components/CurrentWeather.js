import React, {useEffect, useState} from "react";
import {StyleSheet, Text, Image, View} from "react-native";
import {format, isSameDay} from "date-fns";
import {fr} from "date-fns/locale";

const getIcon = (icon) => `http://openweathermap.org/img/wn/${icon}@4x.png`;

export default function CurrentWeather({ data }) {

    const [currentWeather, setCurrentWeather] = useState(null);
    const [temperature, setTemperature] = useState(null);
    const [stringToday, setStringToday] = useState(null);

    useEffect(() => {
        if (data != null) {
            const todayString = format(new Date().getTime() + Math.abs(data.city.timezone * 1000), "EEEE dd MMMM", {locale: fr});
            setStringToday(todayString);

            const currentWeather = data.list.filter(forecast => {
                const today = new Date().getTime() + Math.abs(data.city.timezone * 1000);
                const forecastDate = new Date(forecast.dt * 1000);
                return isSameDay(today, forecastDate);
            })
            setCurrentWeather(currentWeather[0]);
            setTemperature(Math.round(currentWeather[0].main.temp));
        }
    }, [data])

    return (
        <View style={styles.container}>
            <Text style={styles.city}>
                { data?.city?.name }
            </Text>
            <Text style={styles.today}>
                { stringToday }
            </Text>

            <Image
                source={{ uri: getIcon(currentWeather?.weather[0].icon)}}
                style={styles.image}
            />

            <Text style={[styles.temperature, getTemperatureStyle(temperature)]}>
                { temperature }°C
            </Text>
            <Text style={styles.description}>
                { currentWeather?.weather[0].description }
            </Text>
        </View>
    )
}

function getTemperatureStyle(temperature) {
    if (temperature < 5) {
        return {
            color: '#36C8F9',
        }
    } else if (temperature < 10) {
        return {
            color: '#A7B0DD',
        }
    } else if (temperature < 20) {
        return {
            color: '#f5cf65',
        }
    } else if (temperature < 25) {
        return {
            color: '#f2a21e',
        }
    }
    else if (temperature < 30) {
        return {
            color: '#CD8F3E',
        }
    } else {
        return {
            color: '#B24422',
        }
    }
}

const COLOR = "#54565B";

const styles = StyleSheet.create({
    container: {
      marginTop: 60,
      alignItems: 'center',
      height: '65%'
    },
    city: {
        fontSize: 36,
        fontWeight: '700',
        color: COLOR
    },
    today: {
        fontSize: 24,
        fontWeight: '100',
        color: COLOR,
        textTransform: "capitalize",
    },
    image: {
        width: 180,
        height: 180
    },
    temperature: {
        fontSize: 80,
        fontWeight: "bold",
    },
    description: {
        textTransform: 'capitalize',
        fontSize: 24,
        fontWeight: "bold",
        color: COLOR
    }
})