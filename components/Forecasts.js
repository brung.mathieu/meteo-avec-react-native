import React, {useEffect, useState} from "react";
import { View, StyleSheet, ScrollView} from "react-native";
import {format} from "date-fns";
import {fr} from "date-fns/locale";
import ForecastItem from "./ForecastItem";

export default function Forecasts({ data }) {

    const [forecasts, setForecasts] = useState([]);

    useEffect(() => {
        if (data != null) {
            const forecastsData = data.list.map(f => {
                const date = new Date(f.dt * 1000);
                return ({
                    date: date,
                    hour: date.getHours(),
                    temperature: Math.round(f.main.temp),
                    tempMin: Math.round(f.main.temp_min),
                    tempMax: Math.round(f.main.temp_max),
                    icon: f.weather[0].icon,
                    name: format(date, "EEEE", {locale: fr})
                })
            });

            let previousDay = forecastsData[0].name;
            let newTempMin = forecastsData[0].tempMin;
            let newTempMax = forecastsData[0].tempMax;
            let newIcon = forecastsData[0].icon;
            let id = 0;

            let forecastItems = forecastsData.map(f => {
                let day = f.name;
                let hour = f.hour;
                let tempMin = f.tempMin;
                let tempMax = f.tempMax;
                let icon = f.icon;

                if (day == previousDay) {
                    if (tempMin < newTempMin) {
                        newTempMin = tempMin;
                    }
                    if (tempMax > newTempMax) {
                        newTempMax = tempMax;
                    }
                    if (hour === 12 || hour === 13) {
                        newIcon = icon;
                    }

                } else {
                    let object = {
                        id: id,
                        day: previousDay,
                        icon: newIcon,
                        tempMin: newTempMin,
                        tempMax: newTempMax,
                    };

                    previousDay = f.name;
                    newTempMin = f.tempMin;
                    newTempMax = f.tempMax;
                    newIcon = f.icon;
                    id++;

                    return object;
                }

            });
            forecastItems = forecastItems.filter(item => item != undefined);
            forecastItems = forecastItems.filter(item => item.day != forecastsData[0].name);
            setForecasts(forecastItems);
        }

    }, [data])

    return (
        <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.scroll}
        >
            {forecasts.map((f, index) => {
                return (
                    <View key={index} style={styles.forecastContainer}>
                        <ForecastItem forecast={f}/>
                    </View>
                )
            })}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    scroll: {
        width: '100%',
        height: '35%',
    },
    forecastContainer: {
      alignItems: 'center',
    }
});