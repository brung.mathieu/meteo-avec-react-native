import React from "react";
import {View, Text, Image, StyleSheet} from "react-native";

const getIcon = (icon) => `http://openweathermap.org/img/wn/${icon}@2x.png`;

export default function ForecastItem({ forecast }) {
    return (
        <View style={styles.container} >
            <Text style={styles.dayText}>{ forecast.day }</Text>
            <Image
                source={{ uri: getIcon(forecast.icon)}}
                style={styles.image}
            />
            <Text style={[styles.temperature, getTemperatureStyle(forecast.tempMin)]}>Min : { forecast.tempMin }°C</Text>
            <Text style={[styles.temperature, getTemperatureStyle(forecast.tempMax)]}>Max : { forecast.tempMax }°C</Text>
        </View>
    )
}

function getTemperatureStyle(temperature) {
    if (temperature < 5) {
        return {
            color: '#36C8F9',
        }
    } else if (temperature < 10) {
        return {
            color: '#A7B0DD',
        }
    } else if (temperature < 15) {
        return {
            color: '#f5cf65',
        }
    } else if (temperature < 20) {
        return {
            color: '#f2a21e',
        }
    }
    else if (temperature < 25) {
        return {
            color: '#CD8F3E',
        }
    } else {
        return {
            color: '#B24422',
        }
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: 160,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        borderRadius: 50,
        shadowColor: '#000',
        elevation: 4
    },
    dayText: {
        textTransform: "capitalize",
    },
    image: {
        width: 50,
        height: 50,
    },
    temperature: {
        fontSize: 15,
    }
})